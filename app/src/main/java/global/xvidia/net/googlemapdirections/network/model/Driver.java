package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.w3c.dom.Document;

import java.util.List;

/**
 * Created by vasu on 5/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Driver {
    @JsonProperty("id")
    Long id;
    @JsonProperty("driverName")
    String driverName;
    @JsonProperty("driverAddress")
    String driverAddress;
    @JsonProperty("licenseNumber")
    String licenseNumber;
    @JsonProperty("dob")
    String dob;
    @JsonProperty("driverRating")
    int driverRating;
    public Driver(){}
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverAddress() {
        return driverAddress;
    }

    public void setDriverAddress(String driverAddress) {
        this.driverAddress = driverAddress;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(int driverRating) {
        this.driverRating = driverRating;
    }

    public int getDriverExperience() {
        return driverExperience;
    }

    public void setDriverExperience(int driverExperience) {
        this.driverExperience = driverExperience;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<Document> getDriverDocuments() {
        return driverDocuments;
    }

    public void setDriverDocuments(List<Document> driverDocuments) {
        this.driverDocuments = driverDocuments;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    int driverExperience;
    boolean isActive = true;
    boolean isAvailable = true;
    String mobileNumber;
    String remarks;
    List<Document> driverDocuments;
    Car car;
}
