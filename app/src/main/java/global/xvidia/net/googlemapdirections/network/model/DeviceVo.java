package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 11/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceVo {
    @JsonProperty("deviceId")
    String deviceId;
    @JsonProperty("engineSpeed")
    Float engineSpeed;
    @JsonProperty("engineTemperature")
    Float engineTemperature;
    @JsonProperty("engineThrottle")
    Float engineThrottle;
    @JsonProperty("engineRPM")
    Float engineRPM;
    @JsonProperty("engineCoolant")
    Float engineCoolant;
    @JsonProperty("fuelPressure")
    Float fuelPressure;
    @JsonProperty("fuelType")
    String fuelType;
    @JsonProperty("latitude")
    Double latitude;
    @JsonProperty("longitude")
    Double longitude;
    @JsonProperty("speed")
    Float speed;
    @JsonProperty("altitude")
    Float altitude;
    @JsonProperty("lat")
    Float lat;
    @JsonProperty("airIntakeTemperature")
    Float airIntakeTemperature;
    @JsonProperty("ambientAirTemperature")
    Float ambientAirTemperature;
    @JsonProperty("barometricPressure")
    Float barometricPressure;
    @JsonProperty("engineLoad")
    Float engineLoad;
    @JsonProperty("engineRuntime")
    Float engineRuntime;
    @JsonProperty("fuelConsumption")
    Float fuelConsumption;
    @JsonProperty("fuelEconomy")
    Float fuelEconomy;
    @JsonProperty("fuelLevel")
    Float fuelLevel;
    @JsonProperty("intakeManifoldPressure")
    Float intakeManifoldPressure;
    @JsonProperty("massAirFlow")
    Float massAirFlow;
    @JsonProperty("troubleCode")
    String troubleCode;
    @JsonProperty("devcarRegNoiceId")
    String carRegNo;
    @JsonProperty("tripId")
    String tripId;
    @JsonProperty("isOnTrip")
    Boolean isOnTrip;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Float getEngineSpeed() {
        return engineSpeed;
    }

    public void setEngineSpeed(Float engineSpeed) {
        this.engineSpeed = engineSpeed;
    }

    public Float getEngineTemperature() {
        return engineTemperature;
    }

    public void setEngineTemperature(Float engineTemperature) {
        this.engineTemperature = engineTemperature;
    }

    public Float getEngineThrottle() {
        return engineThrottle;
    }

    public void setEngineThrottle(Float engineThrottle) {
        this.engineThrottle = engineThrottle;
    }

    public Float getEngineRPM() {
        return engineRPM;
    }

    public void setEngineRPM(Float engineRPM) {
        this.engineRPM = engineRPM;
    }

    public Float getEngineCoolant() {
        return engineCoolant;
    }

    public void setEngineCoolant(Float engineCoolant) {
        this.engineCoolant = engineCoolant;
    }

    public Float getFuelPressure() {
        return fuelPressure;
    }

    public void setFuelPressure(Float fuelPressure) {
        this.fuelPressure = fuelPressure;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Float getAltitude() {
        return altitude;
    }

    public void setAltitude(Float altitude) {
        this.altitude = altitude;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getAirIntakeTemperature() {
        return airIntakeTemperature;
    }

    public void setAirIntakeTemperature(Float airIntakeTemperature) {
        this.airIntakeTemperature = airIntakeTemperature;
    }

    public Float getAmbientAirTemperature() {
        return ambientAirTemperature;
    }

    public void setAmbientAirTemperature(Float ambientAirTemperature) {
        this.ambientAirTemperature = ambientAirTemperature;
    }

    public Float getBarometricPressure() {
        return barometricPressure;
    }

    public void setBarometricPressure(Float barometricPressure) {
        this.barometricPressure = barometricPressure;
    }

    public Float getEngineLoad() {
        return engineLoad;
    }

    public void setEngineLoad(Float engineLoad) {
        this.engineLoad = engineLoad;
    }

    public Float getEngineRuntime() {
        return engineRuntime;
    }

    public void setEngineRuntime(Float engineRuntime) {
        this.engineRuntime = engineRuntime;
    }

    public Float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(Float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public Float getFuelEconomy() {
        return fuelEconomy;
    }

    public void setFuelEconomy(Float fuelEconomy) {
        this.fuelEconomy = fuelEconomy;
    }

    public Float getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(Float fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public Float getIntakeManifoldPressure() {
        return intakeManifoldPressure;
    }

    public void setIntakeManifoldPressure(Float intakeManifoldPressure) {
        this.intakeManifoldPressure = intakeManifoldPressure;
    }

    public Float getMassAirFlow() {
        return massAirFlow;
    }

    public void setMassAirFlow(Float massAirFlow) {
        this.massAirFlow = massAirFlow;
    }

    public String getTroubleCode() {
        return troubleCode;
    }

    public void setTroubleCode(String troubleCode) {
        this.troubleCode = troubleCode;
    }

    public String getCarRegNo() {
        return carRegNo;
    }

    public void setCarRegNo(String carRegNo) {
        this.carRegNo = carRegNo;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public Boolean getOnTrip() {
        return isOnTrip;
    }

    public void setOnTrip(Boolean onTrip) {
        isOnTrip = onTrip;
    }
}
