package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Ravi on 6/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactData {
    @JsonProperty("contactName")
    private String contactName;
    @JsonProperty("mobileNumber")
    private String mobileNumber;
    private String contactId;

    public ContactData(){}

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }
}
