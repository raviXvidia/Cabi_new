package global.xvidia.net.googlemapdirections.listadapter;

/**
 * Created by vasu on 15/3/16.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import global.xvidia.net.googlemapdirections.EmergencyContactActivity;
import global.xvidia.net.googlemapdirections.FindBuddyActivity;
import global.xvidia.net.googlemapdirections.LoginActivity;
import global.xvidia.net.googlemapdirections.MapsActivity;
import global.xvidia.net.googlemapdirections.MyApplication;
import global.xvidia.net.googlemapdirections.R;
import global.xvidia.net.googlemapdirections.SubscriberActivity;
import global.xvidia.net.googlemapdirections.TripDeatails;
import global.xvidia.net.googlemapdirections.network.ServiceURLManager;
import global.xvidia.net.googlemapdirections.network.VolleySingleton;
import global.xvidia.net.googlemapdirections.network.model.Car;
import global.xvidia.net.googlemapdirections.network.model.ModelManager;
import global.xvidia.net.googlemapdirections.storage.DataStorage;
import global.xvidia.net.googlemapdirections.storage.sqlite.manager.DataCacheManager;
import global.xvidia.net.googlemapdirections.viewelement.CircularImageView;

public class NavDrawerListAdapter extends RecyclerView.Adapter<NavDrawerListAdapter.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private String mNavTitles[];
    private String name;
    private int profile;
    private int icon[];
    private static Context context;


    public class ViewHolder extends RecyclerView.ViewHolder {
       int Holderid;

        public TextView title;
        public ImageView listItemImage;
        public CircularImageView headerProfile;
        public TextView Name;
        public RelativeLayout itemLayout;



       public ViewHolder(View itemView,int ViewType,Context c) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
           super(itemView);
           context = c;
           itemView.setClickable(true);
           // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

           if(ViewType == TYPE_ITEM) {
               title = (TextView) itemView.findViewById(R.id.title);
               listItemImage = (ImageView) itemView.findViewById(R.id.icon);
               itemLayout = (RelativeLayout) itemView.findViewById(R.id.nav_item_layout);
               Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
           }
           else if(ViewType == TYPE_HEADER){

               Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
               headerProfile = (CircularImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
               Holderid = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
           }
       }
   }

    public NavDrawerListAdapter(String Titles[], int Icon[], String Name, int Profile, Context passedContext){ // MyAdapter Constructor with titles and icons parameter
        // titles, icons, name, email, profile pic are passed from the main activity as we
        mNavTitles = Titles;
        name = Name;
        profile = Profile;                     //here we assign those passed values to the values we declared here
        icon = Icon;
        context = passedContext;

        //in adapter

    }

    @Override
    public NavDrawerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false); //Inflating the layout
            ViewHolder vhItem = new ViewHolder(v,viewType,context); //Creating ViewHolder and passing the object of type view

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false); //Inflating the layout

                ViewHolder vhHeader = new ViewHolder(v,viewType,context); //Creating ViewHolder and passing the object of type view

            return vhHeader; //returning the object created


        }
        return null;

    }


    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(NavDrawerListAdapter.ViewHolder holder, final int position) {
        if(holder.Holderid ==1) { // as the list view is going to be called after the header view so we decrement the
            // position by 1 and pass it to the holder while setting the text and image
            holder.title.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
            holder.listItemImage.setImageResource(icon[position-1]);
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MapsActivity.drawer.closeDrawer(Gravity.LEFT);
                    switch (position){
                        case 1:
                            if(ModelManager.getInstance().getPassengerVO() !=null
                                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip()!=null
                                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip().isActive()) {
                                MapsActivity.drawer.closeDrawer(Gravity.LEFT);
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
//                                sendIntent.putExtra(Intent.EXTRA_TEXT, context.getResources().getText(R.string.invite_friends_message,ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp()));
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "Please use the code to view details- CODE: "+ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp());
                                sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getText(R.string.invite_friends_subject));
                                sendIntent.putExtra(Intent.EXTRA_EMAIL, context.getResources().getText(R.string.invite_friends_message,ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp()));
                                sendIntent.setType("text/plain");
                                context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.invite_friends)));
                            }else{
                                showMessageOKCancel(context.getString(R.string.error_share_trip),null);
                            }
                            break;

                        case 2:
                            if(ModelManager.getInstance().getPassengerVO() !=null
                                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip()!=null
                                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip().isActive()) {
                                MapsActivity.drawer.closeDrawer(Gravity.LEFT);
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
//                                sendIntent.putExtra(Intent.EXTRA_TEXT, context.getResources().getText(R.string.invite_friends_message,ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp()));
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "Please use the code to view details- CODE: "+ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp());
                                sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getText(R.string.invite_friends_subject));
                                sendIntent.putExtra(Intent.EXTRA_EMAIL, context.getResources().getText(R.string.invite_friends_message,ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp()));
                                sendIntent.setType("text/plain");
                                context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.invite_friends)));
                            }else{
                                showMessageOKCancel(context.getString(R.string.error_share_trip),null);
                            }
                            break;

                        case 3:
                            if(ModelManager.getInstance().getPassengerVO() !=null
                                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip()!=null
                                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip().isActive()) {
                                MapsActivity.drawer.closeDrawer(Gravity.LEFT);
                                Intent mIntentDetails = new Intent(context, TripDeatails.class);
                                context.startActivity(mIntentDetails);
                            }else{
                                showMessageOKCancel(context.getString(R.string.error_share_trip),null);
                            }

                            break;

                        case 4:

                            Intent mIntentContact = new Intent(context, EmergencyContactActivity.class);
                            context.startActivity(mIntentContact);
                            break;

                        case 5:
                            playlistNameDialog();
                            break;
                        case 6:
                            DataStorage.getInstance().setRemeber(false);
                            DataCacheManager.getInstance().removeContactData();
                            Intent mIntentLogin = new Intent(context, LoginActivity.class);
                            mIntentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(mIntentLogin);
                            ((MapsActivity)context).finish();
                            break;
                    }
                }
            });
        }
        else if(holder.Holderid ==0) {

                    holder.headerProfile.setImageResource(profile);
                // Similarly we set the resources for header view
                    holder.Name.setText(name);

            }
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length+1; // the number of items in the list will be +2 the titles including the header view and
                                    // the footer view .
    }

    private void showMessageOKCancel(final String message, final DialogInterface.OnClickListener okListener) {
        ((MapsActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
        new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
            }
        });
    }
    private void playlistNameDialog(){
        ((MapsActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
//        alert.setTitle(getString(R.string.playlist_text)); //Set Alert dialog title here
                alert.setMessage(context.getResources().getString(R.string.share_code)); //Message here
                final EditText input = new EditText(context);
                input.setSingleLine(true);
                input.setMaxLines(1);
                input.setHint(context.getResources().getString(R.string.share_code));
                input.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

                input.setImeOptions(EditorInfo.IME_ACTION_DONE);
                input.setTextColor(context.getResources().getColor(R.color.colorAccent));
                alert.setView(input, 20, 0, 20, 0);
                        setEditTextMaxLength(input,4);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String srt = input.getEditableText().toString();
                        getOtpDetails(srt);
                        dialog.cancel();
                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
                android.support.v7.app.AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });
    }
    private void setEditTextMaxLength(EditText edt_text,int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        edt_text.setFilters(FilterArray);
    }
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(((MapsActivity) context), permission))
                return false;
        }
        return true;
    }
    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
    private void getOtpDetails(String otp) {
        try {
//            String number = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getOtp();
            String url = ServiceURLManager.getInstance().getOtpDetail(otp);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            Car obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<Car>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                           showProgress(false);
                            if (obj != null) {
                                SubscriberActivity.buddylive = true;
                                ModelManager.getInstance().setBuddycar(obj);
                                Intent mIntentContact = new Intent(context, SubscriberActivity.class);
                                context.startActivity(mIntentContact);

//                                DataStorage.getInstance().setPassword(mPassword);
//                                DataStorage.getInstance().setUsername(mEmail);

//                                Intent intent = new Intent(Ma.this, MapsActivity.class);
//                                startActivity(intent);
//                                finish();
                            } else {
//                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
                        showMessageOKCancel(context.getResources().getString(R.string.error_general), null);
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


        } catch (Exception e) {

        }


    }
    private  void showProgress(final boolean show) {
        ((MapsActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(MapsActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    MapsActivity.mProgressView.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    MapsActivity.mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }
}