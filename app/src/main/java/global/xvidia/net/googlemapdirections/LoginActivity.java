package global.xvidia.net.googlemapdirections;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.xvidia.net.googlemapdirections.network.ServiceURLManager;
import global.xvidia.net.googlemapdirections.network.VolleySingleton;
import global.xvidia.net.googlemapdirections.network.model.Login;
import global.xvidia.net.googlemapdirections.network.model.ModelManager;
import global.xvidia.net.googlemapdirections.network.model.PassengerVO;
import global.xvidia.net.googlemapdirections.storage.DataStorage;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
    // UI references.
    private EditText mNumberView;
    private EditText mPasswordView;
    private ProgressBar mProgressView;
    private ImageView mShowPassword;
    private CheckBox mCheckBoxRemeber;
    private static boolean disableFlag;
    private Button mEmailSignInButton, register;

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        if(DataStorage.getInstance().getRemember()
//                &&!DataStorage.getInstance().getUsername().isEmpty()
//                &&!DataStorage.getInstance().getPassword().isEmpty()){
//            Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putBoolean(AppConsatants.AUTO_LOGIN,true);
//            intent.putExtras(bundle);
//            startActivity(intent);
//            finish();
//        }
        activity = this;
        mNumberView = (EditText) findViewById(R.id.number);
        mShowPassword = (ImageView) findViewById(R.id.login_showpassword);
        mPasswordView = (EditText) findViewById(R.id.password);
        mCheckBoxRemeber = (CheckBox) findViewById(R.id.checkBox_remember);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        showProgress(false);

        mCheckBoxRemeber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                DataStorage.getInstance().setRemeber(isChecked);
            }
        });
        mCheckBoxRemeber.setChecked(DataStorage.getInstance().getRemember());
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        register = (Button) findViewById(R.id.register_button);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    disableFlag = false;
                checkPerm();


            }
        });

        mShowPassword.setVisibility(View.GONE);
        mShowPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final boolean isOutsideView = event.getX() < 0 ||
                        event.getX() > v.getWidth() ||
                        event.getY() < 0 ||
                        event.getY() > v.getHeight();

                // change input type will reset cursor position, so we want to save it
                final int cursor = mPasswordView.getSelectionStart();

                if (isOutsideView || MotionEvent.ACTION_UP == event.getAction())
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                else
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

                mPasswordView.setSelection(cursor);
                return true;
            }
        });

        mPasswordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mShowPassword.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });

        if(DataStorage.getInstance().getRemember()){
            mNumberView.setText(DataStorage.getInstance().getUsername());
            mPasswordView.setText(DataStorage.getInstance().getPassword());
            if(!disableFlag) {
                attemptLogin();
            }
        }

//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, Login.class));
    }

    private void checkPerm() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Coarse location");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Fine location");

        final String[] PERMISSIONS_CONTACT = {Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(android.Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            attemptLogin();
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to Location permission";
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, PERMISSIONS_CONTACT,
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, PERMISSIONS_CONTACT,
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && perms.get(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                attemptLogin();
            } else {
                Toast.makeText(getApplicationContext(), "Location permission has not been granted", Toast.LENGTH_LONG).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(LoginActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mNumberView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mNumberView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            disableFlag = false;
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            disableFlag = false;
            mNumberView.setError(getString(R.string.error_field_required));
            focusView = mNumberView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            disableFlag = false;
            mNumberView.setError(getString(R.string.error_invalid_email));
            focusView = mNumberView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if(!disableFlag) {
                showProgress(true);
                userLoginTask(email, password);

                mNumberView.setEnabled(false);
                mPasswordView.setEnabled(false);
                mEmailSignInButton.setEnabled(false);
                mCheckBoxRemeber.setEnabled(false);
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        if (!email.isEmpty() && email.length() > 2)
            return true;
        else
            return false;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 2;
    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private void userLoginTask(final String username, final String mPassword) {


        try {
            String url = ServiceURLManager.getInstance().getLoginUrl();
            Login login = new Login(username, mPassword);
            login.setUserName(username);
            login.setPassword(mPassword);
            DataStorage.getInstance().setUsername(username);
            DataStorage.getInstance().setPassword(mPassword);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            PassengerVO obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<PassengerVO>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                ModelManager.getInstance().setPassengerVO(obj);

                                DataStorage.getInstance().setPassword(mPassword);
                                DataStorage.getInstance().setUsername(username);

                                Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
                    mNumberView.setEnabled(true);
                    mPasswordView.setEnabled(true);
                    mEmailSignInButton.setEnabled(true);
                    mCheckBoxRemeber.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_incorrect_password), null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(LoginActivity.this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.app_name)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }
}

