package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 5/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class XvidiaDevice {
    @JsonProperty("id")
    Long id;
    @JsonProperty("deviceId")
    String deviceId;
    @JsonProperty("assetId")
    String assetId;
    @JsonProperty("deviceName")
    String deviceName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    Car car;
}
