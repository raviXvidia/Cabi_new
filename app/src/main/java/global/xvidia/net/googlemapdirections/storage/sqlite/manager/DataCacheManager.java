package global.xvidia.net.googlemapdirections.storage.sqlite.manager;


import java.util.ArrayList;

import global.xvidia.net.googlemapdirections.MyApplication;
import global.xvidia.net.googlemapdirections.network.model.ContactData;
import global.xvidia.net.googlemapdirections.storage.sqlite.CacheDataSource;

public class DataCacheManager {

	private static DataCacheManager Instance = null;// new DataCacheManager();
	CacheDataSource dataSource = null;// CachDataSource.getInstance();

	public static DataCacheManager getInstance() {
		if (Instance == null)
			Instance = new DataCacheManager();
		return Instance;
	}

	private DataCacheManager() {
		dataSource = CacheDataSource.getInstance();
	}

	public void open() {
		dataSource.open(MyApplication.getAppContext());
	}

	public void close() {
		dataSource.close();
	}



	 /*
	  Social Auth Facebook DATA
	 */

	/**
	 * Save SocialAuthFacebook data
	 * 
	 * @param obj
	 * @return boolean
	 */

	public boolean saveContactData(ContactData obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.saveContactData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * updates setting Data
	 * 
	 * @param obj SettingData
	 * @return boolean
	 */
	public boolean updateContactData(ContactData obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.updateContactData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	/**
	 * Gets SocialAuthFacebook data
	 * 
	 * @return SocialAuthFacebook
	 */
	public final ContactData getContactData(String userId) {
		dataSource.open(MyApplication.getAppContext());
		ContactData progData = dataSource.getPhoneData(userId);
		dataSource.close();
		if (progData == null) {
			progData = new ContactData();
		}
		return progData;
	}

	public final ArrayList<ContactData> getContactlistData() {
		dataSource.open(MyApplication.getAppContext());
		ArrayList<ContactData> progData = dataSource.getContactlistData();
		dataSource.close();
		if (progData == null) {
			progData = new ArrayList<ContactData>();
		}
		return progData;
	}
	/**
	 * Clears database for a setting
	 * 
	 */
	public final void removeContactData() {
		dataSource.open(MyApplication.getAppContext());
		dataSource.removeContactData();
		dataSource.close();
	}

}
