package global.xvidia.net.googlemapdirections;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.opentok.android.AudioDeviceManager;
import com.opentok.android.BaseAudioDevice;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.opentok.android.SubscriberKit;

import global.xvidia.net.googlemapdirections.fragments.MeterView;
import global.xvidia.net.googlemapdirections.network.model.Car;
import global.xvidia.net.googlemapdirections.network.model.ModelManager;


public class SubscriberActivity extends AppCompatActivity implements
        Session.SessionListener, SubscriberKit.SubscriberListener, PublisherKit.PublisherListener,
        Session.SignalListener, View.OnClickListener {

    private static final String LOG_TAG = SubscriberActivity.class.getSimpleName();
    public static final String SIGNAL_TYPE_CHAT = "chat";
    public static final String SIGNAL_TYPE_IMAGE = "image";

    private String mApiKey;
    private String mStatus;
    private String mSessionPublisherId;
    private String mSessionSubscriberId;
    private String mToken;
    private Session mSession;
    private Subscriber mSubscriber;
    private FrameLayout mSubscriberViewContainer;
    private ImageView imageView;

    private ProgressDialog pDialog;
//    private ImageButton mSendButton;
//    private EditText mMessageEditText;
    private Publisher mPublisher;
    private Animation pulse;
    private ImageView cancelSession;
    private ListView chatView;
    private boolean notificationIntent;
    public static boolean buddylive = false;

//    private ChatMessageAdapter mMessageHistory;
//    private ListView mMessageHistoryListView;
//    private List<ChatMessage> commentList;

//    private static final int NO_OF_EMOTICONS = 54;
//    private LinearLayout emoticonsCover;
//    private PopupWindow popupWindow;
//    private View popUpView;
//    private int keyboardHeight;
//
//    private RelativeLayout parentLayout;
//
//    private boolean isKeyBoardVisible;
//
//    private Bitmap[] emoticons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.subscriber);

//        parentLayout = (RelativeLayout) findViewById(R.id.list_parent);
//        emoticonsCover = (LinearLayout) findViewById(R.id.footer_for_emoticons);
//        popUpView = getLayoutInflater().inflate(R.layout.emoticons_popup, null);
//        chatView = (ListView) findViewById(R.id.message_history_list_view);
//        chatView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                sendImages();
//            }
//        });
//        chatView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (popupWindow.isShowing())
//                    popupWindow.dismiss();
//                return false;
//            }
//        });
        // Defining default height of keyboard which is equal to 230 dip
//        final float popUpheight = getResources().getDimension(
//                R.dimen.keyboard_height);
//        changeKeyboardHeight((int) popUpheight);

        // Showing and Dismissing pop up on clicking emoticons button
//        ImageView emoticonsButton = (ImageView) findViewById(R.id.emoticons_button);
//        emoticonsButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                if (!popupWindow.isShowing()) {
//
//                    popupWindow.setHeight((int) (keyboardHeight));
//
//                    if (isKeyBoardVisible) {
//                        emoticonsCover.setVisibility(LinearLayout.GONE);
//                    } else {
//                        emoticonsCover.setVisibility(LinearLayout.VISIBLE);
//                    }
//                    popupWindow.showAtLocation(parentLayout, Gravity.BOTTOM, 0, 0);
//
//                } else {
//                    popupWindow.dismiss();
//                }
//
//            }
//        });
//        readEmoticons();
//        enablePopUpView();
//        checkKeyboardHeight(parentLayout);
//        enableFooterView();


        cancelSession = (ImageView) findViewById(R.id.cancelLiveSession);
        cancelSession.setImageResource(R.drawable.cross_white);
        cancelSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
/*
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            mSessionPublisherId = intent.getStringExtra(AppConsatants.LIVE_PUBLISHER_ID);
            notificationIntent = intent.getBooleanExtra(AppConsatants.NOTIFICATION_NEXT_INTENT, false);
        }

        if (mSessionPublisherId == null) {
            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
        } else if (mSessionPublisherId.isEmpty()) {
            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
//                    SubscriberActivity.this.finish();
                }
            });
        }*/
//        imageView = (ImageView) findViewById(R.id.imageHeart);
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        mSubscriberViewContainer = (FrameLayout) findViewById(R.id.subscriber_container);
        mSubscriberViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                sendImages();

            }
        });


//        mMessageHistoryListView = (ListView) findViewById(R.id.message_history_list_view);

        // Attach data source to message history

//        commentList = new ArrayList<ChatMessage>();
//        mMessageHistory = new ChatMessageAdapter(this, commentList);
//        mMessageHistoryListView.setAdapter(mMessageHistory);
        // Attach handlers to UI


        pDialog = new ProgressDialog(this);
        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onBackPressed();
            }
        });
        pDialog.setMessage("Connecting...Please wait.");
        pDialog.setCancelable(true);
        showDialog();

        // initialize WebServiceCoordinator and kick off request for necessary data

        /*final View activityRootView = findViewById(R.id.activityRoot);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...


                }
            }
        });*/
    }



    private void callSessionCancel() {
        if (mSession != null)
            mSession.disconnect();
        finish();
    }

    @Override
    public void onBackPressed() {
        restartAudioMode();
//        if (isKeyBoardVisible) {
//            Utils.getInstance().hideKeyboard(this, mMessageEditText);
//        } else if (popupWindow.isShowing()) {
//
//            popupWindow.dismiss();
//
//        } else {
            if (mSession != null) {
                hideDialog();
                mSession.disconnect();
            }
            if (notificationIntent) {
                Intent intent = new Intent(SubscriberActivity.this, MapsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else {
                finish();
            }

            super.onBackPressed();
//        }
    }

/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

    @Override
    protected void onPause() {
        if (mSession != null) {
            hideDialog();
            mSession.disconnect();
            finish();
        }
        super.onPause();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        fetchSubscriberSessionConnectionData();
    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionSubscriberId);
        mSession.setSessionListener(this);
        mSession.setSignalListener(this);
        mSession.connect(mToken);
        ///TO set speaker as audi
        AudioDeviceManager.getAudioDevice().setOutputMode(
                BaseAudioDevice.OutputMode.SpeakerPhone);
    }

    private void logOpenTokError(OpentokError opentokError) {
        Log.e(LOG_TAG, "Error Domain: " + opentokError.getErrorDomain().name());
        Log.e(LOG_TAG, "Error Code: " + opentokError.getErrorCode().name());
    }


//    private void sendImages() {
////        ChatMessage message = new ChatMessage("heart");
//
//        Profile obj = ModelManager.getInstance().getOwnProfile();
//        ChatMessage message = new ChatMessage();
//        message.setMessageText("heart");
//        message.setOwner(obj.getUsername());
//        message.setOwnerDisplayName(obj.getDisplayName());
//        message.setOwnerThumbNail(obj.getThumbNail());
//        mSession.sendSignal(SIGNAL_TYPE_IMAGE, message.toString());
//    }

//    private void disableMessageViews() {
//        mMessageEditText.setEnabled(false);
//        mSendButton.setEnabled(false);
//    }
//
//    private void enableMessageViews() {
//        mMessageEditText.setEnabled(true);
//        mSendButton.setEnabled(true);
//    }

//    private void showMessage(String messageData, boolean remote) {
//        ObjectMapper mapper = new ObjectMapper();
//        ChatMessage message = null;
//        try {
//            message = mapper.readValue(messageData.toString(), ChatMessage.class);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if (commentList == null) {
//            commentList = new ArrayList<>();
//        }
//        if (commentList != null && commentList.size() > 4) {
//            commentList.remove(0);
//        }
//        commentList.add(message);
////        if(mMessageHistory == null){
//        mMessageHistory = new ChatMessageAdapter(this, commentList);
//        mMessageHistoryListView.setAdapter(mMessageHistory);
////        }
//
//        mMessageHistory.notifyDataSetChanged();
////        }
//    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    public void fetchSubscriberSessionConnectionData() {


        Car carObject = ModelManager.getInstance().getCar();
        if(buddylive){
            carObject = ModelManager.getInstance().getBuddycar();
        }
                    if (carObject != null) {
                        mApiKey = carObject.getApiKey();
                        mSessionSubscriberId = carObject.getSessionId();
                        mToken = carObject.getToken();
                        initializeSession();
                    }else {
                        showMessage(getString(R.string.error_live_session_ended), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }

    }

        /* Session Listener methods */

    @Override
    public void onConnected(Session session) {
        Log.i(LOG_TAG, "Session Connected");
        if (mPublisher != null) {
            mSession.publish(mPublisher);
            hideDialog();
        }
//        enableMessageViews();
    }

    @Override
    public void onDisconnected(Session session) {
        Log.i(LOG_TAG, "Session Disconnected");
        Toast.makeText(SubscriberActivity.this,"Session Disconnected",Toast.LENGTH_SHORT).show();
//        disableMessageViews();
//        showMessage("Session Disconnected", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                onBackPressed();
//            }
//        });
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {

        Log.i(LOG_TAG, "Stream Received");
        if (mSubscriber == null) {
            mSubscriber = new Subscriber(this, stream);
            mSubscriber.setSubscriberListener(this);
            mSubscriber.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                    BaseVideoRenderer.STYLE_VIDEO_FILL);
            mSession.subscribe(mSubscriber);
            final MeterView meterView = (MeterView) findViewById(R.id.volume);
            meterView.setIcons(BitmapFactory.decodeResource(getResources(),
                    R.drawable.unmute_sub), BitmapFactory.decodeResource(
                    getResources(), R.drawable.mute_sub));

            meterView.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
                    mSubscriber.setSubscribeToAudio(!view.isMuted());
                }
            });
            mSubscriber
                    .setAudioLevelListener(new SubscriberKit.AudioLevelListener() {
                        @Override
                        public void onAudioLevelUpdated(
                                SubscriberKit subscriber, float audioLevel) {
                            meterView.setMeterValue(audioLevel);
                        }
                    });
        }
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Dropped");
        if (mSubscriber != null) {
            mSubscriber = null;
            mSubscriberViewContainer.removeAllViews();
        }
        showMessage(getString(R.string.error_live_session_ended), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        logOpenTokError(opentokError);
    }

    /* Subscriber Listener methods */

    @Override
    public void onConnected(SubscriberKit subscriberKit) {
        Log.i(LOG_TAG, "Subscriber Connected");
        mSubscriberViewContainer.addView(mSubscriber.getView());

        hideDialog();
//        enableMessageViews();

    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {
        Log.i(LOG_TAG, "Subscriber Disconnected");
//        showMessage("Subscriber Disconnected", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                onBackPressed();
//            }
//        });
    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {
        logOpenTokError(opentokError);
    }

    @Override
    public void onClick(View v) {
//        if (v.equals(mSendButton)) {
////            sendMessage();
//        }
    }

    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
//        boolean remote = !connection.equals(mSession.getConnection());
//        switch (type) {
//            case SIGNAL_TYPE_CHAT:
//                showMessage(data, remote);
//                break;
//            case SIGNAL_TYPE_IMAGE:
//                imageView.setVisibility(View.VISIBLE);
//                imageView.setImageResource(R.drawable.heart_selected);
//                imageView.startAnimation(pulse);
//                pulse.setAnimationListener(new Animation.AnimationListener() {
//
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        imageView.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//                break;
//        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {

    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
      /*  showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onBackPressed();
            }
        });*/
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {

    }

    private void showMessage(String mesg, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(mesg)
                .setPositiveButton(android.R.string.ok, okClicked)
                .setCancelable(false)
                .show();
    }

//    @Override
//    public void keyClickedIndex(final String index) {
//
//        Html.ImageGetter imageGetter = new Html.ImageGetter() {
//            public Drawable getDrawable(String source) {
//                StringTokenizer st = new StringTokenizer(index, ".");
//                Drawable d = new BitmapDrawable(getResources(), emoticons[Integer.parseInt(st.nextToken()) - 1]);
//                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
//                return d;
//            }
//        };
//
//        Spanned cs = Html.fromHtml("<img src ='" + index + "'/>", imageGetter, null);
//
//        int cursorPosition = mMessageEditText.getSelectionStart();
//        mMessageEditText.getText().insert(cursorPosition, cs);
//
//    }
}
