package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 6/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookRide {
    @JsonProperty("car")
    private Car car;
    @JsonProperty("driver")
    private Driver driver;
    @JsonProperty("trip")
    private Trip trip;

    public BookRide(){

    }
    public Car getCar() {

        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public BookRide(Car car, Driver driver, Trip trip) {
        this.car = car;
        this.driver = driver;
        this.trip = trip;
    }

}
