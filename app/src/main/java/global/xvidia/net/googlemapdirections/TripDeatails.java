package global.xvidia.net.googlemapdirections;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import global.xvidia.net.googlemapdirections.network.model.ModelManager;

/**
 * Created by vasu on 11/5/16.
 */
public class TripDeatails extends AppCompatActivity {

    private TextView name, number, dob, address, rating;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        name = (TextView) findViewById(R.id.name);
        number = (TextView) findViewById(R.id.number);
        dob = (TextView) findViewById(R.id.dob);
        address = (TextView) findViewById(R.id.address);
        rating = (TextView) findViewById(R.id.rating);

        if(ModelManager.getInstance().getCar().getDriver() != null){
            String mName = ModelManager.getInstance().getCar().getDriver().getDriverName();
            String mNumber = ModelManager.getInstance().getCar().getDriver().getMobileNumber();
            String mDob = ModelManager.getInstance().getCar().getDriver().getDob();
            String mAddress = ModelManager.getInstance().getCar().getDriver().getDriverAddress();
            int mRating = ModelManager.getInstance().getCar().getDriver().getDriverRating();
            String rat = Integer.toString(mRating);
            name.setText(mName);
            number.setText(mNumber);
            dob.setText(mDob);
            address.setText(mAddress);
            rating.setText(rat);
        }else{
            Toast.makeText(TripDeatails.this,"Driver null",Toast.LENGTH_LONG).show();

        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
