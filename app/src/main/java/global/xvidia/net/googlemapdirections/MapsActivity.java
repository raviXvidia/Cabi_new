package global.xvidia.net.googlemapdirections;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import global.xvidia.net.googlemapdirections.listadapter.NavDrawerListAdapter;
import global.xvidia.net.googlemapdirections.network.ServiceURLManager;
import global.xvidia.net.googlemapdirections.network.VolleySingleton;
import global.xvidia.net.googlemapdirections.network.model.BookRide;
import global.xvidia.net.googlemapdirections.network.model.Car;
import global.xvidia.net.googlemapdirections.network.model.ContactData;
import global.xvidia.net.googlemapdirections.network.model.DirectionsJSONParser;
import global.xvidia.net.googlemapdirections.network.model.LatLong;
import global.xvidia.net.googlemapdirections.network.model.Login;
import global.xvidia.net.googlemapdirections.network.model.ModelManager;
import global.xvidia.net.googlemapdirections.network.model.PassengerVO;
import global.xvidia.net.googlemapdirections.network.model.StopTrip;
import global.xvidia.net.googlemapdirections.network.model.Trip;
import global.xvidia.net.googlemapdirections.storage.DataStorage;
import global.xvidia.net.googlemapdirections.storage.sqlite.manager.DataCacheManager;
import global.xvidia.net.googlemapdirections.utils.AppConsatants;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, PlaceSelectionListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;

    public static ProgressBar mProgressView;
    public static DrawerLayout drawer;
    private static int count = 0;
    private static boolean gps_enabled = false;
    private static boolean network_enabled = false;
    private static int check = 0;
    private static int flag = 0;
    private boolean clicked = false;
    public NavDrawerListAdapter mAdapter;
    ArrayList<LatLng> markerPoints;
    Context mContext;
    FloatingActionButton bookRide;
    MenuItem liveMenuItem;
    int[] ICON = new int[]{
            R.drawable.share_location,
            R.drawable.video_share,
            R.drawable.trip_details,
            R.drawable.phonebook,
            R.drawable.view_friend,
            R.drawable.logout_white,
    };
    String TITLES[] = {
            MyApplication.getAppContext().getResources().getString(R.string.share_location),
            MyApplication.getAppContext().getResources().getString(R.string.share_video),
            MyApplication.getAppContext().getResources().getString(R.string.trip_details),
            MyApplication.getAppContext().getResources().getString(R.string.em_contacts),
            MyApplication.getAppContext().getResources().getString(R.string.view_buddy),
            MyApplication.getAppContext().getResources().getString(R.string.logout),
    };
    String NAME = "Username";
    int PROFILE = R.drawable.user_grey;
    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    private GoogleMap map;
    private TextView mPlaceAttribution, searchTextview;
    private LocationManager locationManager;
    private boolean canGetLocation;
    private Location location;
    private double latitude, longitude;
    private double toLatitude, toLongitude;
    private double fromLatitude, fromLongitude;
    private LatLng origin, destination;
    private Marker currentPosCab;
    private Marker currentPos;
    private Marker destPos;
    private Marker originPos;
    private Activity activity;
    private Toolbar toolbar;                              // Declaring the Toolbar Object
    private ActionBarDrawerToggle mDrawerToggle;
    private FloatingActionButton videoMenu;
    private Button startTrip, stopTrip;
    private LinearLayout searchStartLayout;
    private static Double buddylastKnownLat, buddylastKnownLng;
    GoogleApiClient googleApiClient = null;
    boolean isVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        if (!isGooglePlayServicesAvailable()) {
            Toast.makeText(getApplicationContext(), "GooglePlayServices not available", Toast.LENGTH_LONG).show();
            finish();
        }
        bookRide = (FloatingActionButton) findViewById(R.id.book_my_ride);
        searchStartLayout = (LinearLayout) findViewById(R.id.searchStartLayout);
        searchTextview = (TextView) findViewById(R.id.searchTextview);
        searchTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!clicked) {
                    openDestinationLocationSearchView();
                    clicked = true;
                }
            }
        });
        searchTextview.clearFocus();
        searchStartLayout.setVisibility(View.GONE);
        startTrip = (Button) findViewById(R.id.startTrip);
        stopTrip = (Button) findViewById(R.id.stopTrip);

        startTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callStartTrip();
            }
        });
        stopTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callStopTrip();
            }
        });
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        showProgress(false);
        bookRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                callBookRide();
            }
        });
        videoMenu = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        mContext = MapsActivity.this;
        activity = this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new NavDrawerListAdapter(TITLES, ICON, NAME, PROFILE, MapsActivity.this);
        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
            }
        };
        videoMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ArrayList<ContactData> contactList = new ArrayList<>();
                contactList = DataCacheManager.getInstance().getContactlistData();
                if (contactList.size() == 0) {
                    new android.app.AlertDialog.Builder(MapsActivity.this, R.style.AppCompatAlertDialogStyle)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Em. Contacts")
                            .setMessage("No emergency contacts in your list. Add now.")
                            .setPositiveButton("Add contacts", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getSharedPreferences(DataStorage.MY_PREFERENCES, MODE_PRIVATE).edit().clear().commit();
                                    Intent intent = new Intent(MapsActivity.this, EmergencyContactActivity.class);
                                    startActivity(intent);
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                } else {

                    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                        location = getLocation();
                        String[] emNumbers = new String[contactList.size()];

                        for (int i = 0; i < contactList.size(); i++) {
                            String s = contactList.get(i).getMobileNumber();
                            emNumbers[i] = s;
                        }
                        String message = "";
                        SmsManager sms = SmsManager.getDefault();
                        if (location != null) {
                            message = "Help! My current location is http://maps.google.com/?q=" + location.getLatitude() + "," + location.getLongitude() + " -- Sent from Cabi App";
                        } else {
                            message = "Help! Find me.";
                        }
//                    String numbers[] = {"+919882179477", "+919599957499"};

                        for (String number : emNumbers) {
                            sms.sendTextMessage(number, null, message, null, null);
                        }
                        Toast.makeText(mContext, "Message has been sent to your Em. contacts", Toast.LENGTH_LONG).show();
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_SMS)) {
                            showMessageOKCancel("You need to allow access to SMS permission",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });

                            return;
                        }
                    }

                }

            }
        });

        // drawer Toggle Object Made
        drawer.addDrawerListener(mDrawerToggle); // drawer Listener set to the drawer toggle
        drawer.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
//        View mapView = mapFragment.getView();
//        if (mapView != null &&
//                mapView.findViewById(1) != null) {
        // Get the button view
//        View locationButton = mapFragment.getView().findViewById(2);
        View locationButton = mapFragment.getView().findViewById(Integer.parseInt("2"));
        // and next place it, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                locationButton.getLayoutParams();
        // position on right bottom
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        layoutParams.setMargins(0, 0, 30, 30);


//        markerPoints = new ArrayList<LatLng>();
//         Retrieve the PlaceAutocompleteFragment.
//        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
//                getFragmentManager().findFragmentById(R.id.autocomplete_fragment);
//
//        // Register a listener to receive callbacks when a place has been selected or an error has
//        // occurred.
//        autocompleteFragment.setOnPlaceSelectedListener(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        drawer.closeDrawer(Gravity.LEFT);

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(MapsActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            settingsrequest();
        }

    }

    public void openDestinationLocationSearchView() {

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setCompassEnabled(true);
        check = 1;


//        if(isOn){
//            addCurrentLocationMarker(true);
//        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        startLocationUpdates();

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MapsActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE:

                if (resultCode == RESULT_OK) {

                    clicked = false;
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    LatLng destination = place.getLatLng();
                    toLatitude = destination.latitude;
                    toLongitude = destination.longitude;
                    searchTextview.setText(place.getAddress());
                    map.clear();
                    location = getLocation();
                    drawMarker(location);
                    LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
//        LatLng destination = new LatLng(toLatitude, toLongitude);
                    drawDestPositionMarker(origin, destination);

                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    // TODO: Handle the error.

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }

                break;
        }
    }

    private void startLocationUpdates() {

        if (gps_enabled || !network_enabled) {

//            if (getIntent() != null) {
//                if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean(AppConsatants.AUTO_LOGIN, false)) {
////                    userLoginTask();
////                    stopTrip.setVisibility(View.GONE);
////                    searchStartLayout.setVisibility(View.GONE);
////                    bookRide.setVisibility(View.GONE);
//                } else
            if (ModelManager.getInstance().getPassengerVO() != null
                    && ModelManager.getInstance().getPassengerVO().isTravelling()) {
                stopTrip.setVisibility(View.VISIBLE);
                getEmergencyContact();
                searchStartLayout.setVisibility(View.GONE);
                bookRide.setVisibility(View.GONE);
                callSessionDeatils();
                drawFromToRoute();
            } else if (ModelManager.getInstance().getPassengerVO() != null && !ModelManager.getInstance().getPassengerVO().isTravelling()
                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip() != null
                    && !ModelManager.getInstance().getPassengerVO().getCurrentTrip().isActive()) {
                stopTrip.setVisibility(View.GONE);
                searchStartLayout.setVisibility(View.VISIBLE);
                bookRide.setVisibility(View.GONE);
            } else {
                stopTrip.setVisibility(View.GONE);
                searchStartLayout.setVisibility(View.GONE);
                bookRide.setVisibility(View.VISIBLE);
            }
//            }
//            else {
            location = getLocation();
            if (location != null)
                drawMarker(location);
//            }
//            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
//            map.addMarker(new MarkerOptions().position(origin).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(MapsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void callStartTrip() {

        try {
            String url = ServiceURLManager.getInstance().getStartTripUrl();
            fromLatitude = location.getLatitude();
            fromLongitude = location.getLongitude();
            LatLong latLongObject = new LatLong();
            latLongObject.setFromLat(fromLatitude);
            latLongObject.setFromLong(fromLongitude);
            latLongObject.setToLat(toLatitude);
            latLongObject.setToLong(toLongitude);
            latLongObject.setPassengerPhoneNumber(DataStorage.getInstance().getUsername());

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(latLongObject);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            PassengerVO obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<PassengerVO>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                ModelManager.getInstance().setPassengerVO(obj);
                                stopTrip.setVisibility(View.VISIBLE);
                                searchStartLayout.setVisibility(View.GONE);
                                bookRide.setVisibility(View.GONE);
                                callSessionDeatils();
//                                Intent intent = new Intent(MapsActivity.this, MapsActivity.class);
//                                Bundle bundle = new Bundle();
//                                bundle.putBoolean(AppConsatants.AUTO_LOGIN,false);
//                                intent.putExtras(bundle);
//                                startActivity(intent);
//                                finish();
                            } else {
                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
//                    mEmailView.setEnabled(true);
//                    mPasswordView.setEnabled(true);
//                    mConfirmPasswordView.setEnabled(true);
//                    mEmailSignInButton.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_incorrect_password), null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void callStopTrip() {

        try {
            if (ModelManager.getInstance().getPassengerVO() == null) {
                showError(getString(R.string.error_general), null);
                return;
            }
            if (ModelManager.getInstance().getPassengerVO().getCurrentTrip() == null) {
                showError(getString(R.string.error_general), null);
                return;
            }
            String url = ServiceURLManager.getInstance().getStopTripUrl();
            toLatitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLat();
            toLongitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLong();
            StopTrip latLongObject = new StopTrip();
            latLongObject.setStrId(ModelManager.getInstance().getPassengerVO().getCurrentTrip().getStrId());
            latLongObject.setToLat(toLatitude);
            latLongObject.setToLong(toLongitude);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(latLongObject);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            Trip obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<Trip>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                searchStartLayout.setVisibility(View.GONE);
                                stopTrip.setVisibility(View.GONE);
                                bookRide.setVisibility(View.VISIBLE);
                                if (map != null)
                                    map.clear();
                                location = getLocation();
                                if (location != null)
                                    drawMarker(location);
                                if (liveMenuItem != null)
                                    liveMenuItem.setVisible(false);
                            } else {
                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.e("response", error.toString());
                    showProgress(false);
//                    mEmailView.setEnabled(true);
//                    mPasswordView.setEnabled(true);
//                    mConfirmPasswordView.setEnabled(true);
//                    mEmailSignInButton.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_incorrect_password), null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void callSessionDeatils() {
        if (ModelManager.getInstance().getPassengerVO() == null) {
            showError(getString(R.string.error_general), null);
            return;
        }
        String regNo = ModelManager.getInstance().getPassengerVO().getRegNo();
        String subUrl = ServiceURLManager.getInstance().getLiveSubscriberTokenUrl(regNo);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, subUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    ObjectMapper mapper = new ObjectMapper();
                    Car obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<Car>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setCar(obj);
                        if (liveMenuItem != null && obj.getSessionId() != null && !obj.getSessionId().isEmpty()) {
                            liveMenuItem.setVisible(true);
                        } else {
                            if (liveMenuItem != null)
                                liveMenuItem.setVisible(false);
                        }

                    } else {
                        if (liveMenuItem != null)
                            liveMenuItem.setVisible(false);
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                if (liveMenuItem != null)
                    liveMenuItem.setVisible(false);
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);

    }

    private void callBookRide() {
        try {
            String number = DataStorage.getInstance().getUsername();
            String url = ServiceURLManager.getInstance().getBookCabUrl(number);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}",
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            BookRide obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<BookRide>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                if (!obj.getTrip().isActive()) {

                                    if (bookRide != null) {
                                        bookRide.setVisibility(View.GONE);
                                    }
                                    stopTrip.setVisibility(View.GONE);
                                    searchStartLayout.setVisibility(View.VISIBLE);
                                    bookRide.setVisibility(View.GONE);
                                    callSessionDeatils();
                                } else {
                                    if (bookRide != null) {
                                        bookRide.setVisibility(View.VISIBLE);
                                    }
                                }


//                                DataStorage.getInstance().setPassword(mPassword);
//                                DataStorage.getInstance().setUsername(mEmail);

//                                Intent intent = new Intent(Ma.this, MapsActivity.class);
//                                startActivity(intent);
//                                finish();
                            } else {
//                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
//                    mEmailView.setEnabled(true);
//                    mPasswordView.setEnabled(true);
//                    mEmailSignInButton.setEnabled(true);
//                    mCheckBoxRemeber.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_incorrect_password), null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


        } catch (Exception e) {

        }


    }


    private void getEmergencyContact() {
        try {
            String number = DataStorage.getInstance().getUsername();
            if (number.isEmpty())
                return;
            String url = ServiceURLManager.getInstance().getEmergencyContact(number);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {

                            ObjectMapper mapper = new ObjectMapper();
                            List<ContactData> obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<List<ContactData>>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                if (obj.size() > 0) {
                                    DataCacheManager.getInstance().removeContactData();
                                    for (int i = 0; i < obj.size(); i++) {
                                        String pos = "" + (i + 1);
                                        ContactData data = obj.get(i);
                                        data.setContactId(pos);
                                        DataCacheManager.getInstance().saveContactData(data);
                                    }
                                } else {
                                    DataCacheManager.getInstance().removeContactData();
                                }
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("response", error.toString());
                    showProgress(false);
//                    mEmailView.setEnabled(true);
//                    mPasswordView.setEnabled(true);
//                    mEmailSignInButton.setEnabled(true);
//                    mCheckBoxRemeber.setEnabled(true);
//                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
//                        showError(getString(R.string.error_incorrect_password), null);
//                    } else {
//                        showError(getString(R.string.error_general), null);
//                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


        } catch (Exception e) {

        }


    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MapsActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.app_name)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });

    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(MapsActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            2000,
                            0, this);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                2000,
                                0, this);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    public void drawMarker(Location location) {
        if (location == null) {
            return;
        }
//        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
//        location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
        LatLng latLng = new LatLng(latitude, longitude);
//        markerPoints.add(latLng);
        if (map != null) {
            if (currentPos != null) {
                currentPos.remove();

            }
//            currentPos = map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            map.animateCamera(CameraUpdateFactory.zoomTo(15));

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(bestProvider, 20000, 0, this);
                return;
            }

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        searchTextview.clearFocus();
        clicked = false;
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (gps_enabled || network_enabled) {
            if (check == 1) {
                location = getLocation();
//                LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
//                map.addMarker(new MarkerOptions().position(origin).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                if (location != null)
                    drawMarker(location);
            }
        }
//        getEmergencyContact();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private String getDirectionsUrl(double originLat, double originLong, double destLat, double destLong) {

        String str_origin = "origin=" + originLat + "," + originLong;

        // Destination of route
        String str_dest = "destination=" + destLat + "," + destLong;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private void downloadUrl(final String strUrl) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                strUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String string = response.getString("status");
                    if (string.equalsIgnoreCase("ZERO_RESULTS")) {
                        count++;
                        if (count > 9) {
                            Toast.makeText(getApplicationContext(), "Route cannot be generated", Toast.LENGTH_LONG).show();
                            return;
                        }
                        downloadUrl(strUrl);
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                List<List<HashMap<String, String>>> routes = null;
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(response);

                List<LatLng> points = null;
                PolylineOptions lineOptions = null;

                // Traversing through all the routes
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = routes.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(8);
                    lineOptions.color(Color.BLUE);
                }
                if (map != null) {
                    // Drawing polyline in the Google Map for the i-th route
                    map.addPolyline(lineOptions);
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(origin);
                    builder.include(destination);
                    LatLngBounds bounds = builder.build();

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
                    map.animateCamera(cu, new GoogleMap.CancelableCallback() {
                        @Override
                        public void onFinish() {
//                            CameraUpdate zout = CameraUpdateFactory.zoomBy(-1);
//                            map.animateCamera(zout);
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }

//                hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("APP", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
//                hideDialog();
            }
        });
        // Adding request to request queue
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onPlaceSelected(Place place) {

        LatLng destination = place.getLatLng();
        toLatitude = destination.latitude;
        toLongitude = destination.longitude;

        map.clear();
        location = getLocation();
        drawMarker(location);
        LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
//        LatLng destination = new LatLng(toLatitude, toLongitude);
        drawDestPositionMarker(origin, destination);
//        markerPoints.clear();
//        markerPoints.add(1, destination);


    }

    private void drawDestPositionMarker(LatLng origin, LatLng destination) {
        if (destPos != null) {
            destPos.remove();

        }
        if (originPos != null) {
            originPos.remove();

        }
        try {
            if(map==null)
                return;
            originPos = map.addMarker(new MarkerOptions().position(origin).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//        markerPoints.add(1, origin);
            destPos = map.addMarker(new MarkerOptions().position(destination).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        LatLng ori = markerPoints.get(0);
//        LatLng dest = markerPoints.get(1);

        // Getting URL to the Google Directions API
        this.origin = origin;
        this.destination = destination;

        String url = getDirectionsUrl(origin.latitude, origin.longitude, destination.latitude, destination.longitude);
        downloadUrl(url);
    }

    @Override
    public void onError(Status status) {

        Toast.makeText(this, "Place selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {

        if (currentPosCab != null) {
            currentPosCab.remove();

        }

        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);
        if (location.getAccuracy() < 10.0 && location.getSpeed() < 5.0) {
            MarkerOptions opt = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_marker));
            currentPosCab = map.addMarker(opt);
            // Showing the current location in Google Map
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        }

    }

    private void drawFromToRoute() {
        latitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getFromLat();
        longitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getFromLong();
        toLatitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLat();
        toLongitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLong();

        LatLng origin = new LatLng(latitude, longitude);
        LatLng destination = new LatLng(toLatitude, toLongitude);
        drawDestPositionMarker(origin, destination);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {


    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        liveMenuItem = menu.findItem(R.id.live_view);
        if (ModelManager.getInstance().getPassengerVO() != null && ModelManager.getInstance().getPassengerVO().isTravelling()) {
            liveMenuItem.setVisible(true);
        } else {
            liveMenuItem.setVisible(false);
        }
        return true;
    }


    private void otpDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(MapsActivity.this, R.style.AppCompatAlertDialogStyle);
//        alert.setTitle(getString(R.string.playlist_text)); //Set Alert dialog title here
                alert.setMessage(getResources().getString(R.string.share_code)); //Message here
                final EditText input = new EditText(MapsActivity.this);
                input.setSingleLine(true);
                input.setMaxLines(1);
                input.setHint(getResources().getString(R.string.share_code));
                input.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

                input.setImeOptions(EditorInfo.IME_ACTION_DONE);
                input.setTextColor(getResources().getColor(R.color.colorAccent));
                alert.setView(input, 20, 0, 20, 0);
                setEditTextMaxLength(input, 4);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String srt = input.getEditableText().toString();
                        Intent intent = new Intent(MapsActivity.this, FindBuddyActivity.class);
                        intent.putExtra(AppConsatants.OTP, srt);
                        startActivity(intent);
                        dialog.cancel();
                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });
    }

    /* private void getOtpDetails(String otp) {
         final Handler h = new Handler();
         final int delay = 5000; //milliseconds
         try {
             String url = ServiceURLManager.getInstance().getOtpDetail(otp);
             JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                     new Response.Listener<JSONObject>() {
                         @Override
                         public void onResponse(JSONObject response) {

                             ObjectMapper mapper = new ObjectMapper();
                             Car obj = null;
                             try {
                                 obj = mapper.readValue(response.toString(), new TypeReference<Car>() {
                                 });
                             } catch (IOException e) {
                                 e.printStackTrace();
                             }
                             showProgress(false);
                             if (obj != null) {
 //                                h.postDelayed(new Runnable() {
 //                                    public void run() {
 //                                        callFindBuddy();
 //                                        h.postDelayed(this, delay);
 //                                    }
 //                                }, delay);
                                 String strId = obj.getCurrentTrip().getStrId();
                                 String deviceId = obj.getDevice().getDeviceId();

                                 callFindBuddy(strId, deviceId);
 //                                DataStorage.getInstance().setPassword(mPassword);
 //                                DataStorage.getInstance().setUsername(mEmail);

 //                                Intent intent = new Intent(Ma.this, MapsActivity.class);
 //                                startActivity(intent);
 //                                finish();
                             } else {
 //                                showError(getString(R.string.error_general), null);
                             }

                         }
                     }, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {
                     Log.e("response", error.toString());
                     showProgress(false);
                     showError(getResources().getString(R.string.error_general), null);
                 }
             });
             request.setRetryPolicy(new DefaultRetryPolicy(
                     30000,
                     DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

 //            final JsonObjectRequest requestFinal = request;
             VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


         } catch (Exception e) {

         }


     }

     private void callFindBuddy(String tripId, String deviceId) {
         try {
             String url = ServiceURLManager.getInstance().getDeviceIdUrl(deviceId, tripId);
             JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                     new Response.Listener<JSONObject>() {
                         @Override
                         public void onResponse(JSONObject response) {

                             ObjectMapper mapper = new ObjectMapper();
                             DeviceVo obj = null;
                             try {
                                 obj = mapper.readValue(response.toString(), new TypeReference<DeviceVo>() {
                                 });
                             } catch (IOException e) {
                                 e.printStackTrace();
                             }
                             showProgress(false);
                             if (obj != null) {
                                 if (obj.getOnTrip()) {

                                     if (obj.getLatitude() != null && obj.getLongitude() != null) {
                                         buddylastKnownLat = obj.getLatitude();
                                         buddylastKnownLng = obj.getLongitude();
                                     }
                                     location = getLocation();

                                     LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());

                                     LatLng destination = new LatLng(buddylastKnownLat, buddylastKnownLng);
                                     drawDestPositionMarker(origin, destination);
                                 } else {
                                     new android.app.AlertDialog.Builder(MapsActivity.this, R.style.AppCompatAlertDialogStyle)
                                             .setIcon(android.R.drawable.ic_dialog_alert)
                                             .setTitle("Trip ends")
                                             .setMessage("Your buddy's trip has ended. You cannot track him/her anymore")
                                             .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                 @Override
                                                 public void onClick(DialogInterface dialog, int which) {
                                                     if (location != null)
                                                         drawMarker(location);
                                                 }

                                             })
                                             .show();
                                 }

 //                                DataStorage.getInstance().setPassword(mPassword);
 //                                DataStorage.getInstance().setUsername(mEmail);

 //                                Intent intent = new Intent(Ma.this, MapsActivity.class);
 //                                startActivity(intent);
 //                                finish();
                             } else {
 //                                showError(getString(R.string.error_general), null);
                             }

                         }
                     }, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {
                     Log.e("response", error.toString());
                     showProgress(false);
                     showError(getResources().getString(R.string.error_general), null);
                 }
             });
             request.setRetryPolicy(new DefaultRetryPolicy(
                     30000,
                     DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

 //            final JsonObjectRequest requestFinal = request;
             VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


         } catch (Exception e) {

         }

     }
 */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.live_view) {
            if (ModelManager.getInstance().getPassengerVO() != null
                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip() != null
                    && ModelManager.getInstance().getPassengerVO().getCurrentTrip().isActive()) {
                SubscriberActivity.buddylive = false;
                Intent mIntentContact = new Intent(this, SubscriberActivity.class);
                startActivity(mIntentContact);
            }
            return true;
        } else if (id == R.id.notifications) {

            return true;
        } else if (id == R.id.find_buddy) {
            otpDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setEditTextMaxLength(EditText edt_text, int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        edt_text.setFilters(FilterArray);
    }

    private void userLoginTask() {


        try {
            final String mEmail = DataStorage.getInstance().getUsername();
            final String mPassword = DataStorage.getInstance().getPassword();
            String url = ServiceURLManager.getInstance().getLoginUrl();
            Login login = new Login(mEmail, mPassword);
            login.setUserName(mEmail);
            login.setPassword(mPassword);
            DataStorage.getInstance().setUsername(mEmail);
            DataStorage.getInstance().setPassword(mPassword);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            PassengerVO obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<PassengerVO>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                ModelManager.getInstance().setPassengerVO(obj);

                                DataStorage.getInstance().setPassword(mPassword);
                                DataStorage.getInstance().setUsername(mEmail);
                                Intent intent = new Intent(MapsActivity.this, MapsActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(AppConsatants.AUTO_LOGIN, false);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(MapsActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);

                    Intent intent = new Intent(MapsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
