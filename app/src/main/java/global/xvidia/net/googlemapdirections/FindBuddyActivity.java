package global.xvidia.net.googlemapdirections;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import global.xvidia.net.googlemapdirections.network.ServiceURLManager;
import global.xvidia.net.googlemapdirections.network.VolleySingleton;
import global.xvidia.net.googlemapdirections.network.model.Car;
import global.xvidia.net.googlemapdirections.network.model.DeviceVo;
import global.xvidia.net.googlemapdirections.network.model.DirectionsJSONParser;
import global.xvidia.net.googlemapdirections.utils.AppConsatants;

/**
 * Created by vasu on 11/5/16.
 */
public class FindBuddyActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap map;
    private String otp;
    public static ProgressBar mProgressView;
    private static int count = 0;
    private static Double buddylastKnownLat, buddylastKnownLng;
    private Location location;
    private LocationManager locationManager;
    Context mContext;
    private boolean canGetLocation;
    private double latitude, longitude;
    private Marker currentPosCab;
    private Marker currentPos;
    private Marker destPos;
    private Marker originPos;
    private Toolbar toolbar;
    private LatLng origin, destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_findbuddy);
        mContext = FindBuddyActivity.this;
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (getIntent() != null) {
            otp = getIntent().getStringExtra(AppConsatants.OTP);
        }
        getOtpDetails(otp);
    }

    private void getOtpDetails(String otp) {
        final Handler h = new Handler();
        final int delay = 5000; //milliseconds
        try {
            String url = ServiceURLManager.getInstance().getOtpDetail(otp);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            Car obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<Car>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                final String strId = obj.getCurrentTrip().getStrId();
                                final String deviceId = obj.getDevice().getDeviceId();

                                h.postDelayed(new Runnable() {
                                    public void run() {
                                        callFindBuddy(strId, deviceId);
                                        h.postDelayed(this, delay);
                                    }
                                }, delay);


//                                DataStorage.getInstance().setPassword(mPassword);
//                                DataStorage.getInstance().setUsername(mEmail);

//                                Intent intent = new Intent(Ma.this, MapsActivity.class);
//                                startActivity(intent);
//                                finish();
                            } else {
//                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
                    showError(getResources().getString(R.string.error_general), null);
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


        } catch (Exception e) {

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            map.setMyLocationEnabled(true);
        }

        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    private void callFindBuddy(String tripId, String deviceId) {
        try {
            String url = ServiceURLManager.getInstance().getDeviceIdUrl(deviceId, tripId);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            DeviceVo obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<DeviceVo>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {

                                if (obj.getOnTrip()) {

                                    if (obj.getLatitude() != null && obj.getLongitude() != null) {
                                        buddylastKnownLat = obj.getLatitude();
                                        buddylastKnownLng = obj.getLongitude();
                                    }
                                    location = getLocation();

                                    LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());

                                    LatLng destination = new LatLng(buddylastKnownLat, buddylastKnownLng);
                                    drawDestPositionMarker(origin, destination);
                                } else {
                                    new android.app.AlertDialog.Builder(FindBuddyActivity.this, R.style.AppCompatAlertDialogStyle)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .setTitle("Trip ends")
                                            .setMessage("Your buddy's trip has ended. You cannot track him/her anymore")
                                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }

                                            })
                                            .show();
                                }

//                                DataStorage.getInstance().setPassword(mPassword);
//                                DataStorage.getInstance().setUsername(mEmail);

//                                Intent intent = new Intent(Ma.this, MapsActivity.class);
//                                startActivity(intent);
//                                finish();
                            } else {
//                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
                    showError(getResources().getString(R.string.error_general), null);
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}


        } catch (Exception e) {

        }

    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            2000,
                            0, this);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                2000,
                                0, this);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    public void drawMarker(Location location) {
        if (location == null) {
            return;
        }
//        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
//        location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
        LatLng latLng = new LatLng(latitude, longitude);
//        markerPoints.add(latLng);
        if (map != null) {
            if (currentPos != null) {
                currentPos.remove();

            }
            currentPos = map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            map.animateCamera(CameraUpdateFactory.zoomTo(15));

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(bestProvider, 20000, 0, this);
                return;
            }
        }
    }

    private void drawDestPositionMarker(LatLng origin, LatLng destination) {
        if (destPos != null) {
            destPos.remove();

        }
        if (originPos != null) {
            originPos.remove();

        }
        try {
            originPos = map.addMarker(new MarkerOptions().position(origin).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//        markerPoints.add(1, origin);
            destPos = map.addMarker(new MarkerOptions().position(destination).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        LatLng ori = markerPoints.get(0);
//        LatLng dest = markerPoints.get(1);

        // Getting URL to the Google Directions API
        this.origin = origin;
        this.destination = destination;

        String url = getDirectionsUrl(origin.latitude, origin.longitude, destination.latitude, destination.longitude);
        downloadUrl(url);
    }

    private String getDirectionsUrl(double originLat, double originLong, double destLat, double destLong) {

        String str_origin = "origin=" + originLat + "," + originLong;

        // Destination of route
        String str_dest = "destination=" + destLat + "," + destLong;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private void downloadUrl(final String strUrl) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                strUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String string = response.getString("status");
                    if (string.equalsIgnoreCase("ZERO_RESULTS")) {
                        count++;
                        if (count > 9) {
                            Toast.makeText(getApplicationContext(), "Route cannot be generated", Toast.LENGTH_LONG).show();
                            return;
                        }
                        downloadUrl(strUrl);
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                List<List<HashMap<String, String>>> routes = null;
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(response);

                List<LatLng> points = null;
                PolylineOptions lineOptions = null;

                // Traversing through all the routes
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = routes.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(8);
                    lineOptions.color(Color.BLUE);
                }
                if (map != null) {
                    // Drawing polyline in the Google Map for the i-th route
                    map.addPolyline(lineOptions);
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(origin);
                    builder.include(destination);
                    LatLngBounds bounds = builder.build();

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
                    map.animateCamera(cu, new GoogleMap.CancelableCallback() {
                        @Override
                        public void onFinish() {
//                            CameraUpdate zout = CameraUpdateFactory.zoomBy(-1);
//                            map.animateCamera(zout);
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }

//                hideDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("APP", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
//                hideDialog();
            }
        });
        // Adding request to request queue
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq);
    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(MapsActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (currentPosCab != null) {
            currentPosCab.remove();

        }

        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);
        if (location.getAccuracy() < 10.0 && location.getSpeed() < 5.0) {
            MarkerOptions opt = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.car_marker));
            currentPosCab = map.addMarker(opt);
            // Showing the current location in Google Map
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
            finish();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
