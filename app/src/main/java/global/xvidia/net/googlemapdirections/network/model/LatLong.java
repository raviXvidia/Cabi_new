package global.xvidia.net.googlemapdirections.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 6/5/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LatLong {
    @JsonProperty("strId")
    String strId;
    @JsonProperty("toLat")
    Double toLat;
    @JsonProperty("toLong")
    Double toLong;
    @JsonProperty("fromLat")
    private Double fromLat;
    @JsonProperty("fromLong")
    private Double fromLong;
    @JsonProperty("passengerPhoneNumber")
    private String passengerPhoneNumber;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public Double getFromLat() {
        return fromLat;
    }

    public void setFromLat(Double fromLat) {
        this.fromLat = fromLat;
    }

    public Double getFromLong() {
        return fromLong;
    }

    public void setFromLong(Double fromLong) {
        this.fromLong = fromLong;
    }

    public Double getToLat() {
        return toLat;
    }

    public void setToLat(Double toLat) {
        this.toLat = toLat;
    }

    public Double getToLong() {
        return toLong;
    }

    public void setToLong(Double toLong) {
        this.toLong = toLong;
    }

    public String getPassengerPhoneNumber() {
        return passengerPhoneNumber;
    }

    public void setPassengerPhoneNumber(String passengerPhoneNumber) {
        this.passengerPhoneNumber = passengerPhoneNumber;
    }

}
